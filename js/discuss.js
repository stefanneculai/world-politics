var index = 0;
var tours = [
  {
    "a": 55,
    "d": 5,
    "e": "div.policy-implication-vote-active:first",
    "t": 'Users can express agreement with implications; the highest-rated implications are displayed on the summary page.'
  },
  {
    "a": 145,
    "d": -5,
    "e": "div.policy-implication-tally:first",
    "t": 'Users can express disagreement with benefits. Users can always change their opinion.'
  },
  {
    "a": 105,
    "d": -30,
    "e": "div.policy-implication-text:first",
    "t": 'Discussion points are succinctly summarized.'
  },
  {
    "a": 45,
    "d": 35,
    "e": "div.policy-implication-counter-mark:first",
    "t": 'Multiple counter-points can be raised.'
  },
  {
    "a": 45,
    "d": 35,
    "e": "div.policy-implication-counter-text:first",
    "t": 'Points and counter-points can be improved by editing them inline. All changes are moderated. Users involved in the discussion are notified of changes.'
  },
  {
    "a": 135,
    "d": -5,
    "e": "a.doi-external-link",
    "t": 'Users cite peer-reviewed articles to support claims.'
  },
  {
    "a": 135,
    "d": -5,
    "e": "a.internal-support-link",
    "t": 'All discussion points link to supporting information. Select this link to view an example support page.'
  },
];

function nextTour() {
  $(tours[index].e).grumble({
    text: tours[index].t,
    angle: tours[index].a,
    distance: tours[index].d,
    hideOnClick: true,
    onHide: function( grumble, button ) {
      index++;

      if( index < tours.length ) {
        nextTour();
      }
    }
  });
}

$(document).ready( function() {
  nextTour();
});
