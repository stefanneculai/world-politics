var index = 0;
var tours = [
  {
    "a": 85,
    "e": "body",
    "t": 'This site demonstrates a policy discussion concept. Click each bubble to advance the tour.'
  },
  {
    "a": 180,
    "e": "div.main",
    "t": 'The front page lists upcoming policies.'
  },
  {
    "a": 115,
    "d": 15,
    "e": "div.vote-tally:first",
    "t": 'Policies include the number of people who have voted on it.'
  },
  {
    "a": 115,
    "d": 15,
    "e": "div.view-tally:first",
    "t": 'Policies include the number of people who have visited its summary page.'
  },
  {
    "a": 95,
    "d": -165,
    "e": "div.policy-title:first",
    "t": 'Policy titles link to a summary page.'
  },
  {
    "a": 215,
    "e": "div.policy-tags:first",
    "t": 'Policies are assigned multiple categories (called tags), which users can filter.'
  },
  {
    "a": 120,
    "e": "div.activation:last",
    "t": 'Policies can have an activation date.'
  },
  {
    "a": 85,
    "d": 120,
    "e": "a.policy-title:first",
    "t": 'Click the policy title to view its summary page.'
  },
];

function nextTour() {
  $(tours[index].e).grumble({
    text: tours[index].t,
    angle: tours[index].a,
    distance: tours[index].d,
    hideOnClick: true,
    onHide: function( grumble, button ) {
      index++;

      if( index < tours.length ) {
        nextTour();
      }
    }
  });
}

$(document).ready( function() {
  nextTour();
});
