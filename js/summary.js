var index = 0;
var tours = [
  {
    "a": 269,
    "d": -365,
    "e": "div.policy-vote-calc-pro",
    "t": 'Percentage who agree with the policy.'
  },
  {
    "a": 268,
    "d": -375,
    "e": "div.policy-vote-calc-con",
    "t": 'Percentage who disagree with the policy.'
  },
  {
    "a": 90,
    "d": 20,
    "e": "div.tally-vote-pro",
    "t": 'Agreement vote tally; users click the arrow to express agreement.'
  },
  {
    "a": 90,
    "d": 20,
    "e": "div.tally-vote-con",
    "t": 'Disagreement vote tally; users click the arrow to express disagreement.'
  },
  {
    "a": 180,
    "d": -120,
    "e": "div.policy-overview-text",
    "t": 'Introductory text explains why the policy is needed. Users can directly edit the text to propose changes (subject to moderation).'
  },
  {
    "a": 180,
    "d": -10,
    "e": "div.policy-overview-text a.internal-link:first",
    "t": 'Internal links to more information.'
  },
  {
    "a": 180,
    "d": -10,
    "e": "div.policy-overview-text a.external-link:first",
    "t": 'External links to more information.'
  },
  {
    "a": 180,
    "d": -5,
    "e": "a#policy-feedback-flag",
    "t": 'Users can raise issues with the introductory text that require moderator attention or intervention.'
  },
  {
    "a": 180,
    "d": -5,
    "e": "div.policy-implications ul.policy-implication li:first",
    "t": 'The highest-rated implications from the discussion page.'
  },
  {
    "a": 180,
    "d": -5,
    "e": "a#policy-proposal",
    "t": 'Provides the complete proposal text. Legislative text is presented alongside its layperson translation.'
  },
  {
    "a": 180,
    "d": -5,
    "e": "a#policy-hypothesis",
    "t": 'Describes the measurable goals of the policy used to ascertain its effectiveness.'
  },
  {
    "a": 180,
    "d": -5,
    "e": "a#policy-resources",
    "t": 'Information about the types and amounts of resources required to implement (enact) the policy.'
  },
  {
    "a": 180,
    "d": -5,
    "e": "a#policy-feedback-discuss",
    "t": 'Link to the discussion page where all implications are listed. Click the discuss link to continue.'
  },
];

function nextTour() {
  $(tours[index].e).grumble({
    text: tours[index].t,
    angle: tours[index].a,
    distance: tours[index].d,
    hideOnClick: true,
    onHide: function( grumble, button ) {
      index++;

      if( index < tours.length ) {
        nextTour();
      }
    }
  });
}

$(document).ready( function() {
  nextTour();
});

