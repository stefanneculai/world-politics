var index = 0;
var tours = [
  {
    "a": 150,
    "d": -35,
    "e": "div.support-vote-pro",
    "t": 'Users can vote to agree with a supporting statement.'
  },
  {
    "a": 150,
    "d": -5,
    "e": "div.support-vote-con",
    "t": 'Users can vote to disagree with a supporting statement.'
  },
  {
    "a": 135,
    "d": -5,
    "e": "a.definition",
    "t": 'Users can select hyperlinked definitions to learn contextual meaning.'
  },
  {
    "a": 135,
    "d": -5,
    "e": "a.video",
    "t": 'Links can trigger embedded videos.'
  },
  {
    "a": -15,
    "d": 10,
    "e": "#support-feedback-discuss",
    "t": 'Returns user to discussion page.'
  },
  {
    "a": -15,
    "d": 10,
    "e": "#support-feedback-edit",
    "t": 'Users can suggest edits to supporting text (subject to moderation).'
  },
  {
    "a": -15,
    "d": 10,
    "e": "#support-feedback-flag",
    "t": 'Users can bring support page content to the attention of moderators.'
  },
  {
    "a": 120,
    "d": 10,
    "e": "a.account",
    "t": 'Select the Account link to view an example account.'
  },
];

function nextTour() {
  $(tours[index].e).grumble({
    text: tours[index].t,
    angle: tours[index].a,
    distance: tours[index].d,
    hideOnClick: true,
    onHide: function( grumble, button ) {
      index++;

      if( index < tours.length ) {
        nextTour();
      }
    }
  });
}

$(document).ready( function() {
  nextTour();
});

