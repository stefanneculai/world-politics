#!/bin/bash

# Use XSLT 2.0 to build the example HTML pages from the pages template.
java -jar /home/jarvisd/dev/piechartdemo/saxon9he.jar \
  -s:pages.xml -xsl:pages.xsl

