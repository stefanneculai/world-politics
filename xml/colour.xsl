<?xml version="1.0"?>
<!--
 | The MIT License
 |
 | Copyright 2014 White Magic Software, Inc.
 | 
 | Permission is hereby granted, free of charge, to any person
 | obtaining a copy of this software and associated documentation
 | files (the "Software"), to deal in the Software without
 | restriction, including without limitation the rights to use,
 | copy, modify, merge, publish, distribute, sublicense, and/or
 | sell copies of the Software, and to permit persons to whom the
 | Software is furnished to do so, subject to the following
 | conditions:
 | 
 | The above copyright notice and this permission notice shall be
 | included in all copies or substantial portions of the Software.
 | 
 | THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 | EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 | OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 | NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 | HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 | WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 | FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 | OTHER DEALINGS IN THE SOFTWARE.
 +-->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- Reference white (X, Y, and Z components) -->
<xsl:variable name="X_r" select="0.950456"/>
<xsl:variable name="Y_r" select="1.000000"/>
<xsl:variable name="Z_r" select="1.088754"/>
<xsl:variable name="LAB_EPSILON" select="216.0 div 24389.0"/>
<xsl:variable name="LAB_K" select="24389.0 div 27.0"/>

<!-- Pie wedge colours based on this hue. -->
<xsl:variable name="base_colour" select="'46A5E5'"/>

<!-- Pie wedge stroke colour. -->
<xsl:variable name="stroke_colour" select="'white'"/>

<!--
 | Creates a colour for a particular pie wedge.
 |
 | http://en.wikipedia.org/wiki/HSL_and_HSV 
 +-->
<xsl:template name="fill">
  <!-- Current wedge number for generating a colour. -->
  <xsl:param name="wedge"/>
  <!-- Total number of wedges in the pie. -->
  <xsl:param name="wedges"/>
  <!-- RGB colour in hexadecimal. -->
  <xsl:param name="colour"/>

  <!-- Derive the colour decimal values from $colour's HEX code. -->
  <xsl:variable name="r">
    <xsl:call-template name="hex2dec">
      <xsl:with-param name="hex"
        select="substring( $colour, 1, 2 )"/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="g">
    <xsl:call-template name="hex2dec">
      <xsl:with-param name="hex"
        select="substring( $colour, 3, 2 )"/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="b">
    <xsl:call-template name="hex2dec">
      <xsl:with-param name="hex"
        select="substring( $colour, 5, 2 )"/>
    </xsl:call-template>
  </xsl:variable>

  <!--
   | Convert RGB to XYZ, using nominal range for RGB.
   | http://www.brucelindbloom.com/index.html?Eqn_RGB_to_XYZ.html
   +-->
  <xsl:variable name="r_n" select="$r div 255" />
  <xsl:variable name="g_n" select="$g div 255" />
  <xsl:variable name="b_n" select="$b div 255" />

  <!--
   | Assume colours are in sRGB.
   | http://www.brucelindbloom.com/index.html?Eqn_RGB_XYZ_Matrix.html
   -->
  <xsl:variable name="x"
    select=".4124564 * $r_n + .3575761 * $g_n + .1804375 * $b_n"/>
  <xsl:variable name="y"
    select=".2126729 * $r_n + .7151522 * $g_n + .0721750 * $b_n"/>
  <xsl:variable name="z"
    select=".0193339 * $r_n + .1191920 * $g_n + .9503041 * $b_n"/>

  <!--
   | Convert XYZ to L*a*b.
   | http://www.brucelindbloom.com/index.html?Eqn_XYZ_to_Lab.html
   +-->
  <xsl:variable name="if_x">
    <xsl:call-template name="lab_f">
      <xsl:with-param name="xyz_n" select="$x div $X_r"/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="if_y">
    <xsl:call-template name="lab_f">
      <xsl:with-param name="xyz_n" select="$y div $Y_r"/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="if_z">
    <xsl:call-template name="lab_f">
      <xsl:with-param name="xyz_n" select="$z div $Z_r"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="lab_l" select="(116.0 * $if_y) - 16.0"/>
  <xsl:variable name="lab_a" select="500.0 * ($if_x - $if_y)"/>
  <xsl:variable name="lab_b" select="200.0 * ($if_y - $if_z)"/>
  
  <!--
   | Convert L*a*b to LCH.
   | http://www.brucelindbloom.com/index.html?Eqn_Lab_to_LCH.html
   +-->
  <xsl:variable name="lch_l" select="$lab_l"/>

  <xsl:variable name="lch_c">
    <xsl:call-template name="sqrt">
      <xsl:with-param name="n" select="($lab_a * $lab_a) + ($lab_b * $lab_b)"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="lch_h">
    <xsl:call-template name="atan2">
      <xsl:with-param name="x" select="$lab_b"/>
      <xsl:with-param name="y" select="$lab_a"/>
    </xsl:call-template>
  </xsl:variable>

  <!--
   | Prevent similar adjacent colours.
   |
   | wi = wedge index number
   | wt = total number of wedges
   | wte = evenness determiniate for wt
   |
   | http://math.stackexchange.com/a/936276/7932
   +-->
  <xsl:variable name="wi" select="$wedge"/>
  <xsl:variable name="wt" select="$wedges"/>
  <xsl:variable name="wtm" select="$wt mod 2"/>
  <xsl:variable name="wte" select="$wt + (1 - $wtm)"/>

  <xsl:variable name="w" select="
    round(($wi - $wtm) * floor( $wt div 2 ) mod $wte) + $wtm"/>

  <!-- lch_l, lch_c, and lch_h are now set; rotate the hue. -->
  <xsl:variable name="lch_wedge_h" select="(360.0 div $wedges) * $w"/>

  <!--
   | Convert wedge's hue-adjusted LCH to L*a*b.
   | http://www.brucelindbloom.com/index.html?Eqn_LCH_to_Lab.html
   +-->
  <xsl:variable name="lab_sin_h">
    <xsl:call-template name="sine">
      <xsl:with-param name="degrees" select="$lch_wedge_h"/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="lab_cos_h">
    <xsl:call-template name="cosine">
      <xsl:with-param name="degrees" select="$lch_wedge_h"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="final_lab_l" select="$lch_l"/>
  <xsl:variable name="final_lab_a" select="$lch_c * $lab_cos_h"/>
  <xsl:variable name="final_lab_b" select="$lch_c * $lab_sin_h"/>

  <!--
   | Convert L*a*b to XYZ.
   | http://www.brucelindbloom.com/index.html?Eqn_Lab_to_XYZ.html
   +-->
  <xsl:variable name="of_y" select="($final_lab_l + 16.0) div 116.0"/>
  <xsl:variable name="of_x" select="($final_lab_a div 500.0) + $of_y"/>
  <xsl:variable name="of_z" select="$of_y - ($final_lab_b div 200.0)"/>

  <xsl:variable name="of_x_pow">
    <xsl:call-template name="power">
      <xsl:with-param name="base" select="$of_x"/>
      <xsl:with-param name="exponent" select="3"/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="of_z_pow">
    <xsl:call-template name="power">
      <xsl:with-param name="base" select="$of_z"/>
      <xsl:with-param name="exponent" select="3"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="ox_r">
    <xsl:choose>
      <xsl:when test="$of_x_pow &gt; $LAB_EPSILON">
        <xsl:value-of select="$of_x_pow"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="((116.0 * $of_x) - 16.0) div $LAB_K"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="oy_r">
    <xsl:choose>
      <xsl:when test="$final_lab_l &gt; ($LAB_K * $LAB_EPSILON)">
        <xsl:call-template name="power">
          <xsl:with-param name="base"
            select="($final_lab_l + 16.0) div 116.0"/>
          <xsl:with-param name="exponent"
            select="3"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$final_lab_l div $LAB_K"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="oz_r">
    <xsl:choose>
      <xsl:when test="$of_z_pow &gt; $LAB_EPSILON">
        <xsl:value-of select="$of_z_pow"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="((116.0 * $of_z) - 16.0) div $LAB_K"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="X" select="$ox_r * $X_r"/>
  <xsl:variable name="Y" select="$oy_r * $Y_r"/>
  <xsl:variable name="Z" select="$oz_r * $Z_r"/>

  <!--
   | Convert XYZ to sRGB.
   | http://www.brucelindbloom.com/index.html?Eqn_RGB_XYZ_Matrix.html
   +-->
  <xsl:variable name="R"
    select="3.2404542 * $X + -1.5371385 * $Y + -0.4985314 * $Z"/>
  <xsl:variable name="G"
    select="-0.9692660 * $X + 1.8760108 * $Y + 0.0415560 * $Z"/>
  <xsl:variable name="B"
    select="0.0556434 * $X + -0.2040259 * $Y + 1.0572252 * $Z"/>

  <!-- Round the result. -->
  <xsl:variable name="R_r" select="round( $R * 255 )"/>
  <xsl:variable name="G_r" select="round( $G * 255 )"/>
  <xsl:variable name="B_r" select="round( $B * 255 )"/>

  <xsl:text>rgb(</xsl:text>
  <xsl:value-of select="concat( $R_r, ',', $G_r, ',', $B_r )"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template name="lab_f">
  <xsl:param name="xyz_n"/>

  <xsl:choose>
    <xsl:when test="$xyz_n &gt; $LAB_EPSILON">
      <xsl:call-template name="nthroot">
        <xsl:with-param name="index" select="3"/>
        <xsl:with-param name="radicand" select="$xyz_n"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="($LAB_K * $xyz_n + 16.0) div 116.0" />
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Converts a two-digit hexadecimal number to decimal. -->
<xsl:template name="hex2dec">
  <xsl:param name="hex"/>

  <xsl:variable name="digits" select="'0123456789ABCDEF'"/>
  <xsl:variable name="X" select="substring( $hex, 1, 1 )"/>
  <xsl:variable name="Y" select="substring( $hex, 2, 1 )"/>
  <xsl:variable name="Xval"
    select="string-length(substring-before($digits,$X))"/>
  <xsl:variable name="Yval"
    select="string-length(substring-before($digits,$Y))"/>
  <xsl:value-of select="16 * $Xval + $Yval"/>
</xsl:template>

</xsl:stylesheet>
