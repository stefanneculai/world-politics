<?xml version="1.0" encoding="utf-8"?>
<!--
 | The MIT License
 |
 | Copyright 2014 White Magic Software, Inc.
 | 
 | Permission is hereby granted, free of charge, to any person
 | obtaining a copy of this software and associated documentation
 | files (the "Software"), to deal in the Software without
 | restriction, including without limitation the rights to use,
 | copy, modify, merge, publish, distribute, sublicense, and/or
 | sell copies of the Software, and to permit persons to whom the
 | Software is furnished to do so, subject to the following
 | conditions:
 | 
 | The above copyright notice and this permission notice shall be
 | included in all copies or substantial portions of the Software.
 | 
 | THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 | EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 | OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 | NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 | HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 | WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 | FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 | OTHER DEALINGS IN THE SOFTWARE.
 +-->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:include href="chart.xsl"/>
<xsl:include href="tags.xsl"/>

<xsl:output
  indent="no"
  method="html"
  doctype-system="about:legacy-compat"
  encoding="utf-8"/>

<!-- Action parser that responds to HTTP requests. -->
<xsl:param name="action" select="'do.dhtml'"/>

<xsl:template match="/">
<html>
  <head>
    <meta charset='utf-8'/>
    <title>Liberum Consilium: moderated, transparent deliberation</title>

    <link rel='stylesheet' type='text/css' href='css/common.css'/>
    <link rel='stylesheet' type='text/css'
      href='//fonts.googleapis.com/css?family=Open+Sans'/>
    <link rel='stylesheet' type='text/css'
      href='//fonts.googleapis.com/css?family=Montserrat'/>
  </head>
  <body>
    <xsl:apply-templates/>
    <script type='text/javascript' src='js/common.js'></script>
  </body>
</html>
</xsl:template>

<!-- Make the document complete with div elements and classes. -->
<xsl:template match="*">
  <div class="{local-name()}"><xsl:apply-templates select="node()|@*"/></div>
</xsl:template>

<!-- The 'id' attribute indicates a link. -->
<xsl:template match="*[@id]">
  <div class="{local-name()}"><a
    href="{$action}?action={local-name()}&amp;id={@id}"><xsl:apply-templates
      select="node()|*"/></a></div>
</xsl:template>

<!-- Retain the attributes (except if named "class"). -->
<xsl:template match="@*">
  <xsl:if test="name() != 'class'"><xsl:copy-of select="."/></xsl:if>
</xsl:template>

</xsl:stylesheet>
