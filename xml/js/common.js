var REQ_OBJECTS = [
	function () {return new XMLHttpRequest()},
	function () {return new ActiveXObject('Msxml2.XMLHTTP')},
	function () {return new ActiveXObject('Msxml3.XMLHTTP')},
	function () {return new ActiveXObject('Microsoft.XMLHTTP')}
];

function get_request_object() {
  for( var i = 0; i < REQ_OBJECTS.length; i++ ) {
    try {
      return REQ_OBJECTS[ i ]();
		}
		catch( e ) { continue; }
	}
}

function inject( path, parent, element, callback ) {
  var req = get_request_object();

  req.open( 'GET', path, true );
  
  req.onreadystatechange = function() {
    if( req.readyState == 4 && req.status == 200 ) {
      element.appendChild( document.createTextNode( req.responseText ) );
      parent.appendChild( element );
      
      if( typeof( callback ) === typeof( Function ) ) {
        callback();
      }
    }
  }

  req.send( null );
}

function include( file, type, callback ) {
  var src = type + '/' + file.substr( 0, file.lastIndexOf( '.' ) ) + '.' + type;
  var parent, element;
  
  switch( type ) {
    case 'css':
      parent = document.head || document.getElementsByTagName('head')[0];
      element = document.createElement( 'style' );
      element.type = 'text/' + type;
      element.media = 'all';
      break;
    case 'js':
      parent = document.body || document.getElementsByTagName('body')[0];
      element = document.createElement( 'script' );
      element.type = 'text/javascript';
      break;
  }

  inject( src, parent, element, callback );
}

function path() {
  return location.pathname.split( '/' ).pop();
}

include( path(), 'css' );
include( path(), 'js' );
