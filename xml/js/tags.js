/* ***************************************************************************
 *
 * Miller Columns
 *
 * Transforms an arbitrarily nested unordered list into a series of
 * Miller Columns. The purpose of Miller Columns is to provide a
 * straightforward means to edit hierarchical data. In theory, the
 * same data set could be displayed as a collapsable tree.
 *
 * List items are employed for semantic reasons.
 *
 * The id's must be unique. Usage example:
 *
 * <div class="columns">
 * <ul>
 *   <li id="1">Item A</li>
 *   <li id="2">Item B</li>
 *   <li id="3">Item C
 *     <ul>
 *       <li id="31">Item CA</li>
 *       <li id="32">Item CB</li>
 *       <li id="33">Item CC
 *         <ul>
 *           <li id="331">Item CCA</li>
 *           <li id="332">Item CCB</li>
 *           <li id="333">Item CCC</li>
 *         </ul>
 *       </li>
 *       <li id="34">Item CD</li>
 *       <li id="35">Item CE</li>
 *     </ul>
 *   </li>
 * </ul>
 * </div>
 *
 * Requirements
 * - jQuery 1.11.1
 * - tags.css
 *
 * ***************************************************************************/
(function( $ ) {
  $.fn.millerColumns = function() {
    var $list = $(this).first();
    var $columns = $(this);

    // Breadth-first traversal to rearrange list items into
    // consecutively ordered div wrapper elements.
    while( ($list = $list.children()).length ) {
      $list.each( function( index, element ) {
        var $parent = $(element).parent();

        if( $(element).is( "li" ) ) {
          $parent = $parent.parent();
        }

        // Store the parent id for showing child columns.
        var id = $parent.attr( "id" );

        if( $(element).is( "ul" ) ) {
          // The parent element shall be marked as 0.
          if( id === undefined ) {
            id = 0;
          }

          var $item = $("li#" + id);
          $item.addClass( "parent" );

          $item.on( "click", function() {
            // Hide everything.
            $("div.column[data-parent!=0]").addClass( "collapsed" );
            $("li").removeClass( "selection" );

            // The "id" for the clicked list item becomes the start of
            // the ancestral chain.
            var $child = $("div.column[data-parent=" + id + "]" );
            $child.removeClass( "collapsed" );

            var $li = $("li.parent[id=" + id + "]");
            var $ancestor = $li.parent().parent();
            var ancestor_id = $ancestor.attr( "data-parent" );

            while( ancestor_id !== undefined ) {
              $li.addClass( "selection" );
              $ancestor.removeClass( "collapsed" );

              $li = $("li.parent[id=" + ancestor_id + "]");
              $ancestor = $li.parent().parent();
              ancestor_id = $ancestor.attr( "data-parent" );
            }

            $li.addClass( "selection" );

            var $breadcrumb = $("div.breadcrumb").empty();

            // Add the breadcrumb trail.
            $("li.selection").each( function( _, crumb ) {
              $breadcrumb.append( "<span>" + $(crumb).text() + "</span>" );
            });
          });
          
          var $div = $("<div>").attr( "data-parent", id );
          var $wrapped = $(element).wrapAll( $div );

          // Unnest the list items into contiguous div elements.
          $wrapped.parent().detach().appendTo( $columns ).addClass( "column" );
        }
      });
    }

    $columns.prepend( $("<div>").addClass( "breadcrumb" ) );
    $columns.append( $("<div>").addClass( "toolbar" ) );

    // Hide all the columns except the root.
    $("div.column[data-parent!=0]").addClass( "collapsed" );

    return this;
  };

  $("div.columns").millerColumns();
}(jQuery));

