<?xml version="1.0"?>
<!--
 | The MIT License
 |
 | Copyright 2014 White Magic Software, Inc.
 | 
 | Permission is hereby granted, free of charge, to any person
 | obtaining a copy of this software and associated documentation
 | files (the "Software"), to deal in the Software without
 | restriction, including without limitation the rights to use,
 | copy, modify, merge, publish, distribute, sublicense, and/or
 | sell copies of the Software, and to permit persons to whom the
 | Software is furnished to do so, subject to the following
 | conditions:
 | 
 | The above copyright notice and this permission notice shall be
 | included in all copies or substantial portions of the Software.
 | 
 | THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 | EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 | OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 | NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 | HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 | WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 | FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 | OTHER DEALINGS IN THE SOFTWARE.
 +-->
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:strip-space elements="*"/> 

<xsl:output method="text"/>
<xsl:output method="html" indent="yes" name="html" encoding="utf-8"/>

<xsl:template match="/">
  <xsl:apply-templates />
</xsl:template>

<xsl:template match="page">
<xsl:result-document href="{@file}.html" format="html">
<html>
<head>
  <meta charset="utf-8" />
  <link rel="stylesheet" type="text/css" href="css/tour.css" /> 
</head>
<body>
<div class="header">
  <xsl:if test="position() &gt; 1">
    <a class="page-prev" href="{preceding-sibling::page[1]/@file}.html">Previous</a>
  </xsl:if>
  <xsl:if test="position() &lt; last()">
    <a class="page-next" href="{following-sibling::page[1]/@file}.html">Next</a>
  </xsl:if>
</div>
<hr />
  <iframe id="page" src="{@file}.xml" width="100%" height="100%" frameborder="0">
  </iframe>
</body>
</html>
</xsl:result-document>
</xsl:template>

</xsl:stylesheet>

