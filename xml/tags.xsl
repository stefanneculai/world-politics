<?xml version="1.0"?>
<!--
 | The MIT License
 |
 | Copyright 2014 White Magic Software, Inc.
 | 
 | Permission is hereby granted, free of charge, to any person
 | obtaining a copy of this software and associated documentation
 | files (the "Software"), to deal in the Software without
 | restriction, including without limitation the rights to use,
 | copy, modify, merge, publish, distribute, sublicense, and/or
 | sell copies of the Software, and to permit persons to whom the
 | Software is furnished to do so, subject to the following
 | conditions:
 | 
 | The above copyright notice and this permission notice shall be
 | included in all copies or substantial portions of the Software.
 | 
 | THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 | EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 | OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 | NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 | HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 | WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 | FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 | OTHER DEALINGS IN THE SOFTWARE.
 +-->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- Override the match="*" from the common template by forcing priority. -->
<xsl:template match="taglist" priority="1">
  <div class="columns">
    <ul>
    <xsl:apply-templates mode="tag"/>
    </ul>
  </div>
  <div class="description">
    <h1>Gas</h1>
    <p>
    The <b>gas</b> tag refers to
    <a href="http://en.wikipedia.org/wiki/Natural_gas">natural gas</a>.
    </p>
  </div>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</xsl:template>

<xsl:template match="tag" mode="tag">
<li>
  <xsl:attribute name="id">
    <xsl:value-of select="@id"/>
  </xsl:attribute>
  <xsl:value-of select="name"/>
  <xsl:if test="tag">
    <ul>
    <xsl:apply-templates select="tag" mode="tag"/>
    </ul>
  </xsl:if>
</li>
</xsl:template>

<!-- Ignore any nodes having nothing to do with tags. -->
<xsl:template match="*" mode="tag"/>

<!-- Ignore the tags not in the correct mode. -->
<xsl:template match="taglist"/>

</xsl:stylesheet>
