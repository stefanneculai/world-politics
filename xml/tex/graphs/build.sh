#!/bin/bash

# Replace "// IMPORT" with the contents of the style file.
sed -e '/\/\/ IMPORT/ {
   r style.dot
   d
}' < $1 | dot -Tsvg > $(basename $1 .dot).svg

